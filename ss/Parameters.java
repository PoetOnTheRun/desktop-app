/*
Copyright (C) 2012, Doti, deviatenow.com
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package ss;

public interface Parameters {
	public static final String WEBSITE_URL = "http://ss.deviatenow.com";
	public static final String WEBSITE_SECURE_URL = "https://ss.deviatenow.com";
	public static final String WEBSITE_ONLINEHELP_URL = WEBSITE_SECURE_URL
			+ "/viewtopic.php?f=3&t=175";
	public static final String SCRIPT_FOLDER = "scripts/";
	public static final String IMAGE_FOLDER = "images/";
	public static final String VIDEO_FOLDER = "videos/";
	public static final String SOUND_FOLDER = "sounds/";
	public static final String DEFAULT_PROPERTIES_FILENAME = "data.properties";
	public static final String BACKGROUND_IMAGE_FILENAME = IMAGE_FOLDER
			+ "background.png";
	public static final int API_VERSION = 9;
	public static final int DEFAULT_COLOR = 0xFFEEEE;
	public static final int SSCA_KEY_LIMIT_LENGTH = 1000;
	public static final int SSCA_VALUE_LIMIT_LENGTH = 10000;
}