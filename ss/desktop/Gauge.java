/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.LayoutManager;

import javax.swing.JPanel;

/**
 * Show the tiny rectangle for time lapses
 * 
 * @author Doti
 */
public class Gauge extends JPanel {
	private static final long serialVersionUID = 1L;
	private double prc = 0;
	private Color color;

	public Gauge() {
		super();
	}

	public Gauge(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
	}

	public Gauge(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
	}

	public Gauge(LayoutManager layout) {
		super(layout);
	}

	public double getPrc() {
		return prc;
	}

	public void setPrc(double prc) {
		this.prc = prc;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(color);
		int width = (int) ((prc * getWidth()) / 100);
		g.fillRect(0, 0, width, getHeight());
		int maxR = getHeight() / 4 + 1;
		if (width < getWidth() - maxR) {
			int nbO = 5;
			for (int i = 0; i < nbO; i++) {
				int nbC = 10+i * 20 / nbO;
				int n = (int) (nbC * prc / 100);
				double inN = (nbC * prc / 100 - n);
				int cy = (2 + ((n+1) * (i+1) * 17) % 5) * (getHeight() / 8);
				int cx = (int) (maxR + (n + .5) / nbC * (getWidth() - maxR * 2));
				int r = (int) (inN * maxR);
				g.fillOval(cx - r, cy - r, r * 2, r * 2);
			}
		}
	}

}
