/*
    Copyright (C) 2013, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import java.awt.Cursor;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import ss.MainWindow;
import ss.TextSource;

/**
 * Called by editor.
 * <p>
 * Import script from an URL that goes to a picture gallery
 * </p>
 * <p>
 * The gallery should be a HTML page, with some pictures inside links with names
 * ....jpe?g (ex. : ...melina18.jpeg), or names of an intermediate page with a
 * big picture inside.
 * </p>
 * <p>
 * TODO tests
 * </p>
 * 
 * @author Doti
 */
public class PictureGalleryImportator extends Importator {

	private static final int PICTURE_MIN_HEIGHT = 320;
	private static final int PICTURE_MIN_WIDTH = 320;
	private static final int PICTURE_GALLERY_DELAY = 30;

	@Override
	public String doImport(TextSource textSource, EditorFrame editor) {
		try {

			String res = "";
			String clipboardUrl = editor.getClipboardUrl();
			String url = JOptionPane.showInputDialog(
					textSource.getString("milovana.url"), clipboardUrl);
			if (url == null)
				return null;
			if (!url.contains("://"))
				url = "http://" + url;

			// url = http://girls.twistys.com:8080/preview/421/?t1/revs=tcpsb/in
			// dirUrl = http://girls.twistys.com:8080/preview/421
			// baseUrl = http://girls.twistys.com <- modifified by <base..>
			// rootUrl = http://girls.twistys.com:8080
			String dirUrl = url;
			if (dirUrl.contains("?"))
				dirUrl = dirUrl.substring(0, dirUrl.indexOf("?"));
			dirUrl = dirUrl.substring(0, dirUrl.lastIndexOf("/"));
			String rootUrl = dirUrl;
			if (rootUrl.substring(8).contains("/"))
				rootUrl = rootUrl.substring(0, 8 + rootUrl.substring(8)
						.indexOf("/"));
			int withDelay = JOptionPane.showConfirmDialog(editor,
					textSource.getString("milovana.delay"));
			if (withDelay == JOptionPane.CANCEL_OPTION)
				return null;
			// System.out.println(dirUrl + " " + baseUrl);
			boolean writeDelays = withDelay == JOptionPane.YES_OPTION;
			res += "\n\n// Picture Gallery - " + url + "\n// "
					+ textSource.getString("milovana.rights");
			JOptionPane.showMessageDialog(editor,
					textSource.getString("milovana.please_wait"));
			editor.setCursor(new Cursor(Cursor.WAIT_CURSOR));
			int id = 0;

			java.security.MessageDigest msgDigest = java.security.MessageDigest
					.getInstance("MD5");
			msgDigest.update(url.getBytes());
			for (int i = 0; i < 4; i++)
				id = (id << 8) + msgDigest.digest()[i];
			id = Math.abs(id);
			String imageFolder = "gallery/" + id + "/";
			File f = new File(MainWindow.IMAGE_FOLDER + imageFolder);
			f.mkdirs();

			List<String> imageUrls = new ArrayList<String>();
			editor.addInfo(url);
			String html = editor.getUrlContent(url);

			String txtBasePattern = "<base\\s[^>]*href\\s*=\\s*"
					+ "(\"([^\"]+)\")|" + "('([^']+)')";
			Pattern basePattern = Pattern.compile(txtBasePattern,
					Pattern.CASE_INSENSITIVE);
			Matcher baseMatcher = basePattern.matcher(html);
			String baseUrl = dirUrl;
			if (baseMatcher.find()) {
				if (baseMatcher.group(2) != null)
					baseUrl = baseMatcher.group(2);
				else if (baseMatcher.group(4) != null)
					baseUrl = baseMatcher.group(4);
				if (dirUrl.endsWith("/"))
					baseUrl = baseUrl.substring(0, baseUrl.length() - 1);
			}

			String txtPattern = "<a\\s[^>]*href\\s*=\\s*"
					+ "((\"([^\"]+\\.jpe?g)\")|" + "('([^']+\\.jpe?g)')|"
					+ "([^>]+\\.jpe?g))" + "[^>]*>\\s*" + "<img[^>]*>";
			Pattern pattern = Pattern.compile(txtPattern,
					Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(html);
			while (matcher.find()) {
				if (matcher.group(3) != null)
					imageUrls.add(matcher.group(3));
				else if (matcher.group(5) != null)
					imageUrls.add(matcher.group(5));
				else
					imageUrls.add(matcher.group(6));
			}
			// System.out.println(txtPattern+" => "+imageUrls.size());
			// perhaps try with intermediate pages
			List<String> alreadyDownloadedUrls = new ArrayList<String>();
			if (imageUrls.size() < 3) {
				String txtPattern2 = "<a\\s[^>]*href\\s*=\\s*("
						+ "(\"([^\"]+\\d[^\"]*)\"[^>]*>)|"
						+ "('([^']+\\d[^']*)'[^>]*>)|"
						+ "(([^>]*\\d[^>\\s]*)(\\s[^\\s>]*)*>)"
						+ ")\\s*<img[^>]*>";
				// System.out.println(txtPattern2);
				Pattern pattern2 = Pattern.compile(txtPattern2,
						Pattern.CASE_INSENSITIVE);
				Matcher matcher2 = pattern2.matcher(html);
				List<String> pageUrls = new ArrayList<String>();
				while (matcher2.find()) {
					if (matcher2.group(3) != null)
						pageUrls.add(matcher2.group(3));
					else if (matcher2.group(5) != null)
						pageUrls.add(matcher2.group(5));
					else
						pageUrls.add(matcher2.group(7));
				}
				if (pageUrls.size() >= 3) {
					imageUrls = new ArrayList<String>();
					for (String pageUrl : pageUrls) {
						if (pageUrl.contains("://")
								&& !pageUrl.contains(rootUrl))
							continue;

						String fullUrl = pageUrl;
						if (!pageUrl.contains("://")) {
							if (pageUrl.startsWith("/"))
								fullUrl = rootUrl + fullUrl;
							else
								fullUrl = baseUrl + "/" + fullUrl;
						}

						if (alreadyDownloadedUrls.contains(fullUrl))
							continue;
						alreadyDownloadedUrls.add(fullUrl);
						// System.out.println("Int.pg " + fullUrl + " =>...");

						try {
							String html2 = editor.getUrlContent(fullUrl);
							baseMatcher = basePattern.matcher(html);
							String baseUrl2 = dirUrl;
							if (baseMatcher.find()) {
								if (baseMatcher.group(2) != null)
									baseUrl2 = baseMatcher.group(2);
								else if (baseMatcher.group(4) != null)
									baseUrl2 = baseMatcher.group(4);
								if (dirUrl.endsWith("/"))
									baseUrl2 = baseUrl2.substring(0,
											baseUrl2.length() - 1);
							}

							Pattern patternAllImg = Pattern
									.compile(
											"<img\\s[^>]*src\\s*=\\s*("
													+ "(\"([^\"]+\\.jpe?g)\"[^>]*)|"
													+ "('([^']+\\.jpe?g)'[^>]*)|"
													+ "(([^>]+\\.jpe?g)(\\s[^\\s>]*)*>)"
													+ ")",
											Pattern.CASE_INSENSITIVE);
							Matcher matcherImg = patternAllImg.matcher(html2);
							boolean found = false;
							while (!found && matcherImg.find()) {
								String imgUrl;
								if (matcherImg.group(3) != null)
									imgUrl = matcherImg.group(3);
								else if (matcherImg.group(5) != null)
									imgUrl = matcherImg.group(5);
								else
									imgUrl = matcherImg.group(7);
								String fullUrl2 = imgUrl;
								if (!fullUrl2.contains("://")) {
									if (fullUrl2.startsWith("/"))
										fullUrl2 = rootUrl + fullUrl2;
									else
										fullUrl2 = baseUrl2 + "/" + fullUrl2;
								}
								if (alreadyDownloadedUrls.contains(fullUrl2))
									continue;
								alreadyDownloadedUrls.add(fullUrl2);
								try {
									// System.out.println("Try img : " +
									// fullUrl2);
									URL imgURL = new URL(fullUrl2);
									URLConnection imgURLC = imgURL
											.openConnection();
									imgURLC.setRequestProperty("Referer",
											fullUrl);
									imgURLC.setRequestProperty("Cache-Control",
											"no-cache");
									imgURLC.setRequestProperty("Connection",
											"close");
									imgURLC.setRequestProperty("User-Agent",
											"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1");
									imgURLC.setRequestProperty(
											"Accept-Charset",
											"UTF-8,ISO-8859-1;q=0.7,*;q=0.3");
									imgURLC.setRequestProperty(
											"Accept-Encoding", "deflate,*;q=0");
									imgURLC.setRequestProperty(
											"Accept-Language", "en,en-us;q=0.7");
									InputStream imgURLIS = imgURL.openStream();
									BufferedImage bi = javax.imageio.ImageIO
											.read(imgURLIS);
									imgURLIS.close();
									if (bi != null) {
										// System.out.println(bi.getWidth() +
										// "x" + bi.getHeight());
										if (bi.getWidth() > PICTURE_MIN_WIDTH
												&& bi.getHeight() > PICTURE_MIN_HEIGHT) {
											imageUrls.add(imgUrl);
											found = true;
										}
									}
								} catch (Exception e) {
									System.err.println("Img : " + e.toString());
									// e.printStackTrace();
								}
							}
							// System.out.println("Found ? " + found);
						} catch (Exception e) {
							System.err.println("Int.pg. : " + e.toString());
							// e.printStackTrace();
						}
					}
				}
			}
			if (imageUrls.size() < 3) {
				throw new IllegalArgumentException(
						"Only "
								+ imageUrls.size()
								+ " picture(s) found, invalid gallery. "
								+ "The import tool could probably be updated to accept this gallery, "
								+ "or the authors protected their website, "
								+ "please ask about it in the forum.");
			}
			int n = 0;

			Set<String> images = new HashSet<String>();
			for (String imageUrl : imageUrls) {
				String image = imageUrl;
				if (image.contains("/"))
					image = image.substring(imageUrl.lastIndexOf("/") + 1);
				if (image.contains("?"))
					image = image.substring(0, image.indexOf("?"));
				image = ((n + 1) * 10) + "-" + image;
				f = new File(MainWindow.IMAGE_FOLDER + imageFolder + image);
				OutputStream os = new FileOutputStream(f);
				String fullUrl = imageUrl;
				if (!imageUrl.contains("://")) {
					if (imageUrl.startsWith("/"))
						fullUrl = rootUrl + fullUrl;
					else
						fullUrl = dirUrl + "/" + fullUrl;
				}
				// System.out.println("Image full url : "+fullUrl+" +> "+image);
				InputStream is = null;
				try {
					URL imgURL = new URL(fullUrl);
					URLConnection imgURLC = imgURL.openConnection();
					imgURLC.setRequestProperty("Referer", url);
					imgURLC.setRequestProperty("Cache-Control", "no-cache");
					imgURLC.setRequestProperty("Connection", "close");
					imgURLC.setRequestProperty("User-Agent",
							"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1");
					imgURLC.setRequestProperty("Accept-Charset",
							"UTF-8,ISO-8859-1;q=0.7,*;q=0.3");
					imgURLC.setRequestProperty("Accept-Encoding",
							"deflate,*;q=0");
					imgURLC.setRequestProperty("Accept-Language",
							"en,en-us;q=0.7");
					is = imgURL.openStream();
					int c = 0;
					while ((c = is.read()) > -1)
						os.write(c);
					is.close();
					os.close();
					resizeIfNeeded(f);
					String foundDuplicate = null;
					for (String i : images)
						if (new File(MainWindow.IMAGE_FOLDER + imageFolder + i)
								.length() == f.length())
							foundDuplicate = i;
					if (foundDuplicate == null) {
						images.add(image);
						res += "\nsetImage(\"" + imageFolder + image + "\")";
						res += "\nshow(\"" + (n + 1) + "\")";
						if (writeDelays)
							res += "\nwait(" + PICTURE_GALLERY_DELAY + ")";
						else
							res += "\nshowButton(\"Continue\")";
						res += "\n";
						n++;
					} else {
						f.delete();
					}
				} catch (Exception e) {
					if (is != null)
						is.close();
					if (os != null)
						os.close();
				}
			}
			res += "\nshow(\"Goodbye\")";
			editor.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			editor.addInfo("OK");
			return res;
		} catch (Exception e) {
			editor.addInfo(textSource.getString("error", e.toString()));
			editor.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			e.printStackTrace();
			return null;
		}
	}
}
