/*
Copyright (C) 2012, Doti, deviatenow.com
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package ss.desktop;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * A pretty Swing button
 * 
 * @author Doti
 * 
 */
public class PrettyButton extends JButton {

	private static final long serialVersionUID = 1L;
	private static final double RADIUS_MLT = .6;
	private static final float DARK_BRIGHT_DEFAULT_RATIO = .15f;

	static public Color brighter(Color c, float ratio) {
		int r, g, b;
		if (ratio > 0) {
			r = 255 - (int) (((255 - c.getRed()) * (1 - ratio)));
			g = 255 - (int) (((255 - c.getGreen()) * (1 - ratio)));
			b = 255 - (int) (((255 - c.getBlue()) * (1 - ratio)));
		} else {
			r = (int) (c.getRed() * (1 + ratio));
			g = (int) (c.getGreen() * (1 + ratio));
			b = (int) (c.getBlue() * (1 + ratio));
		}
		return new Color(r, g, b);
	}

	static public Color brighter(Color c) {
		return brighter(c, DARK_BRIGHT_DEFAULT_RATIO);
	}

	static public Color darker(Color c, float ratio) {
		return brighter(c, -ratio);
	}

	static public Color darker(Color c) {
		return brighter(c, -DARK_BRIGHT_DEFAULT_RATIO);
	}

	private Color color;
	private JLabel l;

	public PrettyButton(String text, Color color) {
		super(text);
		setColor(color);
		l = new JLabel();
		l.setVisible(false);
	}

	@Override
	protected void paintComponent(Graphics graphics) {
		Graphics2D g = (Graphics2D) graphics;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		Color cUp, cDown, cLow;

		if (getModel().isPressed()) {
			cUp = darker(color);
			cLow = brighter(color, 0.08f);
			cDown = brighter(color);
		} else if (getModel().isRollover()) {
			cUp = brighter(color, .25f);
			cLow = brighter(color, .02f);
			cDown = darker(color, .05f);
		} else {
			cUp = brighter(color, 0.2f);
			cLow = darker(color, 0.08f);
			cDown = darker(color);
		}
		if (!hasFocus()) {
			cUp = darker(cUp, 0.2f);
			cLow = darker(cLow);
			cDown = darker(cDown);
		}
		g.setPaint(new GradientPaint(10, 0, cUp, 10, getHeight(), cDown));
		int radius = (int) (getHeight() * RADIUS_MLT);
		g.fillRoundRect(0, 0, getWidth() - 1, getHeight() - 1, radius, radius);

		g.setPaint(cLow);
		for (int x = 4; x < getWidth() - 4; x++)
			g.fillRect(x, getHeight()
					- 2
					- (((x ^ 5) * 17 + (x ^ 21) * 11 + (x ^ 10) * 7 + (x ^ 3)
							* 5 + x * x) % 9), 1, 1);

		l.setFont(getFont());
		l.setText(getText());
		Dimension d = l.getPreferredSize();
		g.translate(getWidth() / 2 - d.width / 2, 0);
		l.setSize(d.width, getHeight());
		if (darkMean())
			l.setForeground(brighter(cUp, .7f));
		else
			l.setForeground(darker(cUp, .7f));
		l.paint(g);
		g.translate(-getWidth() / 2 + d.width / 2, 0);
	}

	@Override
	protected void paintBorder(Graphics graphics) {
		Graphics2D g = (Graphics2D) graphics;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		if (darkMean()) {
			g.setColor(brighter(color, .3f));
		} else {
			g.setColor(darker(color, .3f));
		}
		g.setStroke(new BasicStroke(1));
		int radius = (int) (getHeight() * RADIUS_MLT);
		g.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, radius, radius);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	private boolean darkMean() {
		return color.getRed() + color.getGreen() + color.getBlue() < 127 * 3;
	}
}
