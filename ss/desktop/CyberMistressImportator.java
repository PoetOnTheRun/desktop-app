/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

import org.ini4j.Ini;
import org.ini4j.Wini;

import ss.TextSource;

/**
 * Called by editor.
 * <p>
 * Import script from CyberMistress (Freya ?); tested on 3 scripts with success.
 * </p>
 * <p>
 * Some things ignored, like "nameMistress", or "setTimer"
 * </p>
 * 
 * @author Doti
 */
public class CyberMistressImportator extends Importator {

	private static final String[] IGNORED_SECTIONS_ASSIGNMENTS = { "Monday",
			"Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
	private static final String[] IGNORED_TAGS = { "allowedMessage",
			"assignments", "deleteTimer", "freezeTimer", "launch", "log",
			"maxAttitude", "minAttitude", "nameMistress", "nameSlave",
			"randomAssignments", "randomInterval", "remember", "reportBox",
			"setINI", "setTimer", "title", "unfreezeTimer" };

	@Override
	public String doImport(TextSource textSource, EditorFrame editor) {
		try {

			JFileChooser fc;
			try {
				fc = new JFileChooser();
			} catch (Exception ex) {
				try {
					UIManager
							.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
				} catch (Exception e) {
					if (e instanceof RuntimeException)
						throw (RuntimeException) e;
					else
						e.printStackTrace();
				}
				fc = new JFileChooser();
			}
			fc.addChoosableFileFilter(new FileFilter() {
				String desc;

				public String getDescription() {
					return desc;
				}

				public boolean accept(File f) {
					return f.isDirectory()
							|| f.getName().toLowerCase().endsWith(".cm");
				}

				public FileFilter init(String desc) {
					this.desc = desc;
					return this;
				}
			}.init(textSource.getString("cybermistress.ext_descr")));
			int res = fc.showOpenDialog(editor);
			Main.setLookAndFeel(true);
			if (res != JFileChooser.APPROVE_OPTION)
				return null;

			Wini ini = new Wini();
			ini.getConfig().setLowerCaseOption(true); //some CM scripts are not well formed enough
			ini.setFile(fc.getSelectedFile());
			ini.load();
			return translate(ini, textSource);
		} catch (Exception e) {
			editor.addInfo(textSource.getString("error", e.toString()));
			e.printStackTrace();
			return null;
		}
	}

	private String changeMessage(String s, String scriptName, Wini ini,
			TextSource textSource) {
		// setValue accepts number expressions, but also strings
		// string : if not something that'll be allowed as an expression.
		// Not in an expression : letter/num space letter/num,
		// dot space? letter, letter space? dot, ...
		boolean isString = s.matches(".*[0-9a-zA-Z]\\s+[0-9a-zA-Z].*")
				|| s.matches(".*\\.\\s*[a-zA-Z].*")
				|| s.matches(".*[a-zA-Z]\\s*\\..*") || s.matches("\\s*");
		// \->\\, "->\", \\n->\n
		s = s.replace("\\", "\\\\").replace("\"", "\\\"")
				.replace("\\\\n", "\\n").replace("$", "\\$");
		s = s.replace("#Path", "#path");
		s = s.replace("#path",
				"\"+new java.io.File(\".\").getCanonicalPath()+\"");
		s = s.replace("#Slave", "#slave");
		s = s.replace("#slave", "\"+loadString(\"intro.name\")+\"");
		s = s.replace("#Mistress", "#mistress");
		s = s.replace("#mistress",
				textSource.getString("cybermistress.mistress"));
		s = s.replace("#Attitude", "#attitude");
		s = s.replace("#attitude", "2");
		s = s.replace("#year", "\"+Calendar.getInstance().get("
				+ "Calendar.YEAR)+\"");
		s = s.replace("#month", "\"+Calendar.getInstance().get("
				+ "Calendar.MONTH)+\"");
		s = s.replace("#day", "\"+Calendar.getInstance().get("
				+ "Calendar.DAY_OF_MONTH)+\"");
		s = s.replace("#hour", "\"+Calendar.getInstance().get("
				+ "Calendar.HOUR_OF_DAY)+\"");
		s = s.replace("#minute", "\"+Calendar.getInstance().get("
				+ "Calendar.MINUTE)+\"");
		s = s.replace("#second", "\"+Calendar.getInstance().get("
				+ "Calendar.SECOND)+\"");
		if (s.contains("\""))
			isString = true;
		if (isString)
			s = s.replaceAll("#value\\(([^\\)]*)\\)", "\"+loadString(\""
					+ scriptName + ".$1\")+\"");
		else
			s = s.replaceAll("#value\\(([^\\)]*)\\)", "loadInteger(\""
					+ scriptName + ".$1\")");
		// numbered
		s = s.replaceAll("#random\\[([^\\]]*)\\]", "getRandom($1)");
		s = s.replaceAll("(#(\\w+)[^\\w])", "\"+loadString(\"" + scriptName
				+ ".$2\")+\"");
		s = s.replaceAll("\\{", "<em>");
		s = s.replaceAll("\\}", "</em>");
		s = s.replaceAll("\\\\\\s", "\\n");
		if (isString)
			s = "\"" + s + "\"";

		// ""+a -> a, but \""+a !-> \a
		s = s.replace("\\\"\"+", "$specialquoted$").replace("\"\"+", "")
				.replace("$specialquoted$", "\\\"\"+");
		s = s.replace("+\"\"", "");
		return s;
	}

	private String translate(Wini ini, TextSource textSource) {
		String scriptName = ini.getFile().getName();
		scriptName = scriptName.substring(0, scriptName.lastIndexOf("."));
		scriptName = scriptName.replace(" ", "_");
		boolean punishmentScript = scriptName.contains("punishments");
		boolean assignmentScript = scriptName.contains("assignments");
		String content = "";
		String startBlock = ini.fetch("General", "start");
		if (startBlock == null)
			startBlock = ini.fetch("general", "start");
		String endBlock = ini.fetch("General", "end");
		if (endBlock == null)
			endBlock = ini.fetch("general", "end");
		int defNum = 1;
		int blockNum = 0;
		String pointsMessage = "";
		List<String> sections = new ArrayList<String>();
		List<Integer> severities = new ArrayList<Integer>();
		for (String sectionName : ini.keySet())
			if (!sectionName.equals("General")) {
				Ini.Section s = ini.get(sectionName);
				sectionName = sectionName.replaceAll("[\"\\\\]", "'");
				if (punishmentScript) {
					s.add("message", sectionName);
					sectionName = "section" + blockNum;
					if (s.containsKey("severity"))
						severities.add(Integer.parseInt(s.get("severity")));
				}
				boolean ignored = false;
				if (assignmentScript)
					for (String ignoredSectionAssiments : IGNORED_SECTIONS_ASSIGNMENTS)
						if (sectionName.trim().equals(ignoredSectionAssiments))
							ignored = true;
				if (ignored)
					continue;

				content += "case \"" + sectionName + "\":\n";
				sections.add(sectionName);

				if (s.containsKey("requiresflag") && s.containsKey("else")) {
					int sufBlockNum = 0;
					// | and one of them is false : else
					// / and all of them are false : else
					content += "\tdef rfcount" + blockNum + " = 0\n";
					for (String sv : s.get("requiresflag").split("\\||/")) {
						content += "\tdef flag" + blockNum + "_" + sufBlockNum
								+ "=loadInteger(\"" + scriptName + "." + sv
								+ "\")\n" + "\tif(flag" + blockNum + "_"
								+ sufBlockNum + "!=null && " + "getTime()<flag"
								+ blockNum + "_" + sufBlockNum + ") rfcount"
								+ blockNum + "++\n";
						sufBlockNum++;
					}
					if (s.get("requiresflag").contains("|"))
						content += "\tif( rfcount" + blockNum + "<"
								+ sufBlockNum + " ) {\n\t\tblock=\""
								+ s.get("else") + "\"\n\t\tbreak\n\t}\n";
					else
						content += "\tif( rfcount" + blockNum
								+ "==0) {\n\t\tblock=\"" + s.get("else")
								+ "\"\n\t\tbreak\n\t}\n";
				}
				if (s.containsKey("notifflag") && s.containsKey("else")) {
					int sufBlockNum = 1000;
					// | and all of them are true : else
					// / and one of them is true : else
					content += "\tdef nicount" + blockNum + " = 0\n";
					for (String sv : s.get("notifflag").split("\\||/")) {
						content += "\tdef flag" + blockNum + "_" + sufBlockNum
								+ "=loadInteger(\"" + scriptName + "." + sv
								+ "\")\n" + "\tif(flag" + blockNum + "_"
								+ sufBlockNum + "!=null && " + "getTime()<flag"
								+ blockNum + "_" + sufBlockNum + ") nicount"
								+ blockNum + "++\n";
						sufBlockNum++;
					}
					if (s.get("notifflag").contains("|"))
						content += "\tif( nicount" + blockNum + "=="
								+ sufBlockNum + " ) {\n\t\tblock=\""
								+ s.get("else") + "\"\n\t\tbreak\n\t}\n";
					else
						content += "\tif( nicount" + blockNum
								+ ">0) {\n\t\tblock=\"" + s.get("else")
								+ "\"\n\t\tbreak\n\t}\n";
				}
				// clothes, toys, isvalue like requiresflag
				if (s.containsKey("clothes") && s.containsKey("else")) {
					int sufBlockNum = 1100;
					// | and one of them is false : else
					// / and all of them are false : else
					content += "\tdef clcount" + blockNum + " = 0\n";
					for (String sv : s.get("clothes").split("\\||/")) {
						content += "\tif(loadBoolean(\"clothes."
								+ sv.toLowerCase().replace(" ", "_")
								+ "\")==true) clcount" + blockNum + "++\n";
						sufBlockNum++;
					}
					if (s.get("clothes").contains("|"))
						content += "\tif( clcount" + blockNum + "<"
								+ sufBlockNum + " ) {\n\t\tblock=\""
								+ s.get("else") + "\"\n\t\tbreak\n\t}\n";
					else
						content += "\tif( clcount" + blockNum
								+ "==0) {\n\t\tblock=\"" + s.get("else")
								+ "\"\n\t\tbreak\n\t}\n";
				}
				if (s.containsKey("toys") && s.containsKey("else")) {
					int sufBlockNum = 1100;
					// | and one of them is false : else
					// / and all of them are false : else
					content += "\tdef tocount" + blockNum + " = 0\n";
					for (String sv : s.get("toys").split("\\||/")) {
						content += "\tif(loadBoolean(\"toys."
								+ sv.toLowerCase().replace(" ", "_")
								+ "\")==true) tocount" + blockNum + "++\n";
						sufBlockNum++;
					}
					if (s.get("toys").contains("|"))
						content += "\tif( tocount" + blockNum + "<"
								+ sufBlockNum + " ) {\n\t\tblock=\""
								+ s.get("else") + "\"\n\t\tbreak\n\t}\n";
					else
						content += "\tif( tocount" + blockNum
								+ "==0) {\n\t\tblock=\"" + s.get("else")
								+ "\"\n\t\tbreak\n\t}\n";
				}
				// sample : isValue=a(footy shorts)|a(boots)
				if (s.containsKey("isvalue") && s.containsKey("else"))
					content += "\tif("
							+ s.get("isvalue")
									.replace("\"", "\\\"")
									.replaceAll(
											"([^\\(\\)\\|]*)\\(([^\\)]*)\\)",
											"loadString(\"$1\")!=\"$2\"")
									.replace("/", "&&") + ") {\n\t\tblock=\""
							+ s.get("else") + "\"\n\t\tbreak\n\t}\n";
				if (s.containsKey("maxvalue") && s.containsKey("else"))
					content += "\tif("
							+ s.get("maxvalue").replaceAll(
									"^(.*)\\(([^\\)]*)\\).*$",
									"loadInteger(\"$1\")>=\"$2\") {")
							+ "\n\t\tblock=\"" + s.get("else")
							+ "\"\n\t\tbreak\n\t}\n";
				if (s.containsKey("minvalue") && s.containsKey("else"))
					content += "\tif("
							+ s.get("minvalue").replaceAll(
									"^(.*)\\(([^\\)]*)\\).*$",
									"loadInteger(\"$1\")<=\"$2\") {")
							+ "\n\t\tblock=\"" + s.get("else")
							+ "\"\n\t\tbreak\n\t}\n";
				String[] sets = { "set", "setvalue", "changevalue" };
				for (String set : sets)
					if (s.containsKey(set)) {
						for (String sv : s.get(set).split("\\|")) {
							Pattern p = Pattern
									.compile("^([^\\(]*)\\((.*)\\)[^\\)]*$");
							Matcher m = p.matcher(sv);
							if (m.find())
								sv = "\tsave(\""
										+ scriptName
										+ "."
										+ m.group(1)
										+ "\","
										+ changeMessage(m.group(2), scriptName,
												ini, textSource) + ")\n";
							sv = sv.replaceAll("askText:([^\"]*)",
									"\"+getString(\"$1\",\"\")+\"");
							content += sv;
						}
					}
				if (s.containsKey("deletevalue")) {
					content += "\t//TODO perhaps correct this\n";
					for (String sv : s.get("deletevalue").split("\\|"))
						content += "\tsave(\"" + scriptName + "." + sv
								+ "\", null)\n";
				}
				String[] credits = { "points", "negotiationscredits" };
				for (String credit : credits)
					if (s.containsKey(credit)) {
						content += "\tif(loadInteger(\"cybermistress.points\")==null)"
								+ "\n\t\tsave(\"cybermistress.points\", 80)"
								+ "\n\tsave(\"cybermistress.points\", "
								+ s.get(credit)
								+ "+loadInteger(\"cybermistress.points\"))\n";
						pointsMessage = "\"(You've got \"+loadInteger(\"cybermistress.points\")+\" points)\\n\"+";
					}
				if (s.containsKey("picture")) {
					String picStr = s.get("picture").replace("\\", "/")
							.replace(" ", "_").replace("*", "");
					if (s.get("picture").contains("|")) {
						content += "\tdef pictures" + blockNum + " = [\n";
						int nb = 0;
						for (String pic : picStr.split("\\|")) {
							content += "\t\t"
									+ changeMessage(pic, scriptName, ini,
											textSource) + ", \n";
							nb++;
						}
						content += "\t\"\"]\n";
						content += "\tsetImage(pictures" + blockNum + "["
								+ "getRandom(" + nb + ")])\n";
					} else
						content += "\tsetImage("
								+ changeMessage(picStr, scriptName, ini,
										textSource) + ")\n";
				}
				if (s.containsKey("play"))
					content += "\tplayBackgroundSound(\""
							+ s.get("play").replace("\\", "/")
									.replace(" ", "_").replace("*", "")
									.replace("sounds/", "") + "\")\n";
				if (s.containsKey("importfile")) {
					String[] infos = s.get("importfile").split("\\|");
					String todo = "TODO this is only a part of importfile";
					if (infos.length > 1)
						todo += " : " + infos[1];
					content += "\t//" + todo + "\n" + "\tsave(\"" + scriptName
							+ ".file" + defNum + "\", getFile(\"" + infos[0]
							+ "\"))\n";
					defNum++;
				}
				String[] messageTags = { "message", "assignment1",
						"assignment2", "assignment3", "assignment4",
						"assignment5" };
				int sufBlockNum = 4000;
				for (String messageTag : messageTags) {
					if (s.containsKey(messageTag)) {
						String message = s.get(messageTag);
						if (message.contains("|")) {
							String[] messages = message.split("\\|");
							String sep = "";
							String messagesTxt = "";
							for (String mess : messages) {
								messagesTxt += sep
										+ changeMessage(mess, scriptName, ini,
												textSource);
								sep = ",";
							}
							content += "\tdef messages" + blockNum + "_"
									+ sufBlockNum + " = [" + messagesTxt
									+ "]\n" + "\tshow(" + pointsMessage
									+ "messages" + blockNum + "_" + sufBlockNum
									+ "[getRandom(" + messages.length + ")])\n";
						} else {
							message = changeMessage(message, scriptName, ini,
									textSource);
							content += "\tshow(" + pointsMessage + message
									+ ")\n";
						}
						pointsMessage = "";
						if (assignmentScript && !message.trim().equals(""))
							content += "\tshowButton(\"Ok\")\n";
					}
					sufBlockNum++;
				}
				if (s.containsKey("delay"))
					if (s.get("delay").toLowerCase().equals("play"))
						content += "\tplaySound(\""
								+ s.get("play").replace("\\", "/")
										.replace(" ", "_") + "\")\n";
					else if (s.get("delay").toLowerCase().startsWith("button"))
						content += "\tshowButton("
								+ changeMessage(
										s.get("delay")
												.substring(
														s.get("delay").indexOf(
																"|") + 1),
										scriptName, ini, textSource) + ")\n";
					else if (s.containsKey("report")
							&& s.containsKey("reportlate")) {
						int time = 10;
						try {
							time = Integer.parseInt(s.get("delay"));
						} catch (Exception e) {
						}
						content += "\tif(showButton("
								+ changeMessage(s.get("report"), scriptName,
										ini, textSource) + ")>" + time + ")\n"
								+ "\t\tblock = \"" + s.get("reportlate")
								+ "\"\n" + "\telse\n\t"; // then block=... from
						// next=
					} else if (s.get("delay").toLowerCase()
							.startsWith("random")) {
						int minRandom = 0;
						try {
							minRandom = Integer.parseInt(changeMessage(
									s.get("mindelay"), scriptName, ini,
									textSource));
						} catch (Exception e) {
						}
						int maxRandom = 10;
						try {
							maxRandom = Integer.parseInt(changeMessage(
									s.get("maxdelay"), scriptName, ini,
									textSource));
						} catch (Exception e) {
						}
						content += "\twait(" + minRandom + "+getRandom("
								+ (maxRandom - minRandom) + "))\n";
					} else
						content += "\twait("
								+ changeMessage(s.get("delay"), scriptName,
										ini, textSource) + ")\n";
				if (s.containsKey("flag")) {
					String[] flags = s.get("flag").split("\\|");
					for (String flag : flags) {
						String end = "getTime()+365*24*3600";
						if (flag.contains("{")) {
							if (flag.contains("{date}"))
								end = "(Math.floor(getTime()/(24*3600))+1)*24*3600";
							else if (flag.contains("s}")) {
								String f2 = flag
										.substring(flag.indexOf("{") + 1);
								if (f2.contains("lasts:"))
									f2 = f2.substring(f2.indexOf("lasts:") + 6);
								f2 = f2.substring(0, f2.indexOf("s"));
								end = f2 + "+getTime()";
							} else if (flag.contains("m}")) {
								String f2 = flag
										.substring(flag.indexOf("{") + 1);
								if (f2.contains("lasts:"))
									f2 = f2.substring(f2.indexOf("lasts:") + 6);
								f2 = f2.substring(0, f2.indexOf("m"));
								end = "getTime()+60*" + f2;
							} else if (flag.contains("h}")) {
								String f2 = flag
										.substring(flag.indexOf("{") + 1);
								if (f2.contains("lasts:"))
									f2 = f2.substring(f2.indexOf("lasts:") + 6);
								f2 = f2.substring(0, f2.indexOf("h"));
								end = "getTime()+3600*" + f2;
							} else
								content += "\t// partly ignored : flag=" + flag
										+ "\n";
							flag = flag.substring(0, flag.indexOf("{"));
						}
						content += "\tsave(\"" + scriptName + "." + flag
								+ "\", " + end + ")\n";
					}
				}
				if (s.containsKey("unflag")) {
					String[] flags = s.get("unflag").split("\\|");
					for (String flag : flags)
						content += "\tsave(\"" + scriptName + "." + flag
								+ "\", null)\n";
				}
				if (s.containsKey("next"))
					content += getGroovyRandomBlock(s.get("next"),
							s.get("random"));
				if (s.containsKey("askYN") && s.containsKey("yes")
						&& s.containsKey("no")) {
					String[] messages = s.get("askYN").split("\\|");
					String message = changeMessage(messages[0], scriptName,
							ini, textSource);
					String yesContent = getGroovyRandomBlock(s.get("yes"),
							s.get("random"));
					String noContent = getGroovyRandomBlock(s.get("no"),
							s.get("random"));
					if (messages.length < 3)
						content += "\tif(getBoolean(" + message + ")) {\n"
								+ "\t" + yesContent + "\t} else {\n\t"
								+ noContent + "\t}\n";
					else
						content += "\tif(getBoolean(" + message + ",\""
								+ messages[1] + "\",\"" + messages[2]
								+ "\")) {\n" + "\t\t" + yesContent
								+ "\t} else {\n" + noContent + "\t}\n";
				}
				if (s.containsKey("routine")) {
					String[] parts = s.get("routine").split("\\|");
					if (parts.length > 1)
						content += "\tsave(\"cybermistress.wanted_start_block\","
								+ "\"" + parts[1].trim() + "\")\n";
					content += "\treturn \""
							+ parts[0].toLowerCase().replace("\\", "/")
									.replace(".cm", "") + "\"\n";
				}
				if (sectionName.equals(endBlock)) {
					content += "\tendReached = true\n";
				}

				for (String ignoredTag : IGNORED_TAGS)
					if (s.containsKey(ignoredTag))
						content += "\t//ignored : " + ignoredTag + "="
								+ s.get(ignoredTag) + "\n";
				if (s.containsKey("severity") && !punishmentScript)
					content += "\t//ignored : severities=" + s.get("severity")
							+ "\n";
				content += "\tbreak\n\n";
				blockNum++;
			}
		if (startBlock == null && endBlock == null) {
			startBlock = "specialStart";
			String sectionsTxt = "";
			String sep = "";
			for (String section : sections) {
				sectionsTxt += sep + "\"" + section + "\"";
				sep = ",";
			}
			content += "// recognised as special script (no \"general\" section)\n"
					+ "case \"specialStart\":"
					+ "\n\tdef sections = ["
					+ sectionsTxt + "]";

			if (punishmentScript) {
				String severitiesTxt = "";
				sep = "";
				for (Integer severity : severities) {
					severitiesTxt += sep + severity;
					sep = ",";
				}
				content += "\n\tdef severities = ["
						+ severitiesTxt
						+ "]"
						+ "\n\tdef n = 0"
						+ "\n\tif(loadInteger(\"cybermistress.punishement\")==null)"
						+ "\n\t\tif(getBoolean(\"Do you ask for a severe punishment ?\"))"
						+ "\n\t\t\tif(getBoolean(\"Do you ask for a very severe punishment ?\"))"
						+ "\n\t\t\t\tif(getBoolean(\"Really severe ?\"))"
						+ "\n\t\t\t\t\tn = 10"
						+ "\n\t\t\t\telse"
						+ "\n\t\t\t\t\tn = 8"
						+ "\n\t\t\telse"
						+ "\n\t\t\t\tn = 6"
						+ "\n\t\telse if(getBoolean(\"Do you ask for a very light punishment ?\"))"
						+ "\n\t\t\t\tn = 2"
						+ "\n\t\t\telse"
						+ "\n\t\t\t\tn = 4"
						+ "\n\telse"
						+ "\n\t\tn = loadInteger(\"punishement\")"
						+ "\n\tdef found = null"
						+ "\n\twhile(found==null) {"
						+ "\n\t\tdef v = getRandom(sections.size)"
						+ "\n\t\tif(severities[v]<=n+1 && severities[v]>=n+1) found = v"
						+ "\n\t}"
						+ "\n\tsave(\"cybermistress.punishment\",null)"
						+ "\n\tblock = sections[found]" + "\n\tbreak\n\n";
			} else if (assignmentScript) {
				content += "\n\tshow(\"Your assignment for today...\")"
						+ "\n\twait(3)"
						+ "\n\tblock = null"
						+ "\n\tdef dateStr = new java.text.SimpleDateFormat(\"dd/MM\").format(new Date())"
						+ "\n\tfor(def section:sections)"
						+ "\n\t\tif(section==dateStr)"
						+ "\n\t\t\tblock = section"
						+ "\n\tdef nSeed = 0"
						+ "\n\twhile(block==null) {"
						+ "\n\t\tdef nRandom = (Calendar.getInstance().get(Calendar.DAY_OF_YEAR)*13+"
						+ "\n\t\t\tCalendar.getInstance().get(Calendar.YEAR)*19+"
						+ "\n\t\t\tnSeed*23)%"
						+ sections.size()
						+ "\n\t\tif(sections[nRandom].length()>5) //avoid dates"
						+ "\n\t\t\tblock = sections[nRandom]" + "\n\t\tnSeed++"
						+ "\n\t}" + "\n\tbreak\n\n";
			} else {
				content += "\n\tdef selected = getSelectedValue(\"Please select...\",sections)"
						+ "\n\tblock = sections[selected]" + "\n\tbreak\n\n";
			}
		}
		return "\n// CyberMistress - "
				+ scriptName
				+ "\n//\n"
				+ "def endReached = false\n"
				+ "def block = \""
				+ startBlock
				+ "\"\n"
				+ "if(loadString(\"cybermistress.wanted_start_block\")!=null) {\n"
				+ "\tblock = loadString(\"cybermistress.wanted_start_block\")\n"
				+ "\tsave(\"cybermistress.wanted_start_block\",null)\n" + "}\n"
				+ "def newBlock = block\n"
				+ "while(!endReached && newBlock!=null) {\n" + "block = null\n"
				+ "switch(newBlock) {\n" + content + "default:\n"
				+ "\tshow(\"Unfinished ? (\"+block+\")\")\n"
				+ "\tendReached = true\n} // end switch \n"
				+ "newBlock = block\n" + "} // end while";
	}

	private String getGroovyRandomBlock(String value, String random) {
		String content = "";
		if (value.toLowerCase().equals("random") || value.contains("|")) {
			String[] randoms = null;
			if (value.toLowerCase().equals("random"))
				randoms = random.trim().split("\\|");
			else
				randoms = value.trim().split("\\|");
			content += "\tswitch(getRandom(" + randoms.length + ")) {\n";
			for (int i = 0; i < randoms.length; i++)
				content += "\tcase " + i + ": block = \"" + randoms[i]
						+ "\"; break\n";
			content += "\t}\n";
		} else {
			content += "\tblock = \"" + value + "\"\n";
		}
		return content;
	}
}
