/*
    Copyright (C) 2017, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import java.util.HashMap;

/**
* Simple command line arguments parser 
* @author Doti
*/
public class ArgumentParser extends HashMap<String, String> {
	private static final long serialVersionUID = 1L;

	public ArgumentParser(String[] args) {
		for(String arg : args) {
			if(arg.startsWith("-")) {
				if(arg.contains("=")) {
					String key = arg.substring(1).substring(0,arg.indexOf("=")-1);
					String value = arg.substring(arg.indexOf("=")+1);
					put(key, value);
				} else {
					String key = arg.substring(1);
					put(key, "true");
				}
			} else if(arg.startsWith("/")) {
				if(arg.contains("=")) {
					String key = arg.substring(1).substring(0,arg.indexOf("=")-1);
					String value = arg.substring(arg.indexOf("=")+1);
					put(key, value);
				} else {
					String key = arg.substring(1);
					put(key, "true");
				}
			} else {
				put("default", arg);
			}
		}
	}
}
