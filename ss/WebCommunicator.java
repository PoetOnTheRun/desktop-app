/*
    Copyright (C) 2015, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 * Send, receive data and files for scripts, using HTTP post
 * 
 * @author Doti
 */
public class WebCommunicator {

	private static final String PROXY_URL = Parameters.WEBSITE_SECURE_URL
			+ "/proxy.php";
	private static final int SESSION_LIMITER_THREASHOLD = 50;

	private int nbSession;

	static {
		try {
			//buggy J7 / SLL ?
			//System.setProperty("jsse.enableSNIExtension", "false");
			
			//no more TlSv1.0 ?
			java.lang.System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");			
			SSLContext sslCtx = SSLContext.getInstance("TLS");
			TrustManager[] trustManagers = new TrustManager[] { new X509TrustManager() {
				// TODO more checks
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String t) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String t) {
				}
			} };
			sslCtx.init(null, trustManagers, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sslCtx
					.getSocketFactory());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		HttpURLConnection.setFollowRedirects(true);
		HttpsURLConnection.setFollowRedirects(true);
	}

	public WebCommunicator() {
		this.nbSession = 0;
	}

	/**
	 * return bytes read
	 */
	public int sendAndSaveToFile(Map<String, String> data, List<String> files,
			String filename) throws IOException {
		InputStream is = sendAndGetInputStream(data, files);
		FileOutputStream fos = new FileOutputStream(filename);
		byte[] buffer = new byte[1024];
		int bytesRead;
		int res = 0;
		while ((bytesRead = is.read(buffer)) != -1) {
			fos.write(buffer, 0, bytesRead);
			res += bytesRead;
		}
		fos.close();
		is.close();
		if(res==0)
			new File(filename).delete();
		return res;
	}

	public String send(Map<String, String> data, List<String> files)
			throws IOException {
		InputStream is = sendAndGetInputStream(data, files);
		BufferedReader br = new BufferedReader(new InputStreamReader(is,
				Charset.forName("utf-8")));
		String allResponse = "";
		String line;
		while ((line = br.readLine()) != null)
			allResponse += line + "\n";
		br.close();
		is.close();
		return allResponse.trim();
	}

	public HttpURLConnection getConnection(String urlString) throws IOException {
		HttpURLConnection conn = null;
		// Java 1.6 is buggy
		if (urlString.contains("https://") && System.getProperty("java.version").startsWith("1.6")) {
			urlString = urlString.replace("https://", "http://");
		}
        URL url = new URL(urlString);
        conn = (HttpURLConnection) url.openConnection();
		if (conn.getURL().getProtocol().contains("https")) {
			((HttpsURLConnection) conn)
					.setHostnameVerifier(new HostnameVerifier() {

						@Override
						public boolean verify(String hostname,
								SSLSession sslSession) {
							return hostname.equals(sslSession.getPeerHost());
						}
					});
		}
		return conn;
	}

	private InputStream sendAndGetInputStream(Map<String, String> data,
			List<String> files) throws IOException {

		if (!data.containsKey("action"))
			throw new IllegalArgumentException("No action");

		try {
			Thread.sleep(1000 * (int) (nbSession / SESSION_LIMITER_THREASHOLD));
		} catch (InterruptedException e) {
		}
		nbSession++;

		boolean noMoreRedirects = false;
		int redirects = 0;
		HttpURLConnection conn = null;
		String url = PROXY_URL;
		while (!noMoreRedirects && redirects < 16) {
			conn = getConnection(url);

			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setReadTimeout(120 * 1000);
			conn.setRequestProperty("Accept",
					"text/html,application/xhtml+xml,"
							+ "application/xml;q=0.9,*/*;q=0.8");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data; boundary=BoBoBaBa");
			conn.setRequestProperty("Accept-Encoding", "deflate");

			// Open now, see Tim Bray
			OutputStream os = conn.getOutputStream();
			PrintWriter pw = new PrintWriter(os, true);

			for (String key : data.keySet()) {
				pw.write("\n--BoBoBaBa\n");
				pw.write("Content-Disposition: form-data; name=\"" + key + "\"\n");
				pw.write("Content-Type: text/plain; charset=UTF-8\n\n");
				pw.write(data.get(key));
			}
			pw.flush();

			int n = 0;
			if (files != null)
				for (String filename : files) {
					String ext = filename.substring(filename.lastIndexOf("."));
					pw.write("\n--BoBoBaBa\n"
							+ "Content-Disposition: form-data; name=\"file" + n
							+ "\"; filename=\"" + n + ext + "\n"
							+ "Content-Transfert-encoding: binary\n"
							+ "Content-Type: image/jpeg\n\n");
					pw.flush();

					FileInputStream fis = new FileInputStream(filename);
					byte[] buffer = new byte[1024];
					int bytesRead;
					while ((bytesRead = fis.read(buffer)) != -1)
						os.write(buffer, 0, bytesRead);
					os.flush();
					fis.close();
					n++;
				}

			pw.write("\n--BoBoBaBa--\n");
			pw.flush();
			
			noMoreRedirects = true;
			if (conn.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM
					|| conn.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP
					|| conn.getResponseCode() == HttpURLConnection.HTTP_SEE_OTHER) {
				url = conn.getHeaderField("Location");
				noMoreRedirects = false;
				redirects++;
			}
		}

		// untested if(conn.getResponseCode()!=HttpURLConnection.HTTP_OK)
		// untested throw new
		// IOException("HTTP response code invalid : "+conn.getResponseCode());
		InputStream is = conn.getInputStream();
		return is;
	}

	/**
	 * @return the nbSession
	 */
	public int getNbSession() {
		return nbSession;
	}

	/**
	 * @param nbSession
	 *            the nbSession to set
	 */
	public void setNbSession(int nbSession) {
		this.nbSession = nbSession;
	}

	public Date getOnlineModifiedDate(OnlineModifiedFileType type) {
		try {
			URL url = null;
			switch (type) {
			case APK:
				url = new URL(Parameters.WEBSITE_URL + "/files/sexscripts.apk");
				break;
			default:
				url = new URL(Parameters.WEBSITE_URL + "/files/sexscripts.zip");
			}
			URLConnection conn = url.openConnection();
			long lastModified = conn.getLastModified();
			if (lastModified == 0)
				return null;
			return new Date(lastModified);
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public enum OnlineModifiedFileType {
		ZIP, APK
	};
}
